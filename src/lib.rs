use libc::c_char;
use std::ffi::{CStr, CString};
#[derive(Debug, Clone)]
#[repr(C)]
pub enum Kind {
    Ok,
    Error,
}
#[repr(C)]
struct _MyStuffPrivate {
    message: *const c_char,
    kind: Kind,
}

#[repr(C)]
pub struct MyStuff {
    inner: _MyStuffPrivate,
}

#[no_mangle]
pub unsafe extern "C" fn my_stuff_new(apa: *const c_char) -> MyStuff {
    let mut kind = Kind::Error;
    let message = match CStr::from_ptr(apa).to_str() {
        Ok(a) => {
            kind = Kind::Ok;
            CString::new(a).unwrap().into_raw() as *const c_char
        }
        Err(e) => CString::new(format!("Failed cause: {}", e))
            .unwrap()
            .into_raw() as *const c_char,
    };
    MyStuff {
        inner: _MyStuffPrivate { message, kind },
    }
}

#[no_mangle]
pub extern "C" fn my_stuff_message(my: &MyStuff) -> *const c_char {
    my.inner.message
}

#[no_mangle]
pub extern "C" fn my_stuff_kind(my: &MyStuff) -> Kind {
    my.inner.kind.clone()
}

#[no_mangle]
pub extern "C" fn kind_as_string(kind: &Kind) -> *const c_char {
    CString::new(format!("{:?}", kind)).unwrap().into_raw() as *const c_char
}
