#include "ccbind.h"
#include <string>
using namespace std;
int main(int _argc, char **_argv) {
  string s("apadd");
  MyStuff my = my_stuff_new(s.c_str());
  Kind kind = my_stuff_kind(&my);
  printf("%s %d %s\n", my_stuff_message(&my), my_stuff_kind(&my), kind_as_string(&kind));
  return 0;
}

