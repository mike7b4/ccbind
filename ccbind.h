#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <ostream>
#include <new>

enum class Kind {
  Ok,
  Error,
};

struct _MyStuffPrivate {
  const char *message;
  Kind kind;
};

struct MyStuff {
  _MyStuffPrivate inner;
};

extern "C" {

MyStuff my_stuff_new(const char *apa);

const char *my_stuff_message(const MyStuff *my);

Kind my_stuff_kind(const MyStuff *my);

const char *kind_as_string(const Kind *kind);

} // extern "C"
